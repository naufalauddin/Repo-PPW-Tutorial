$( document ).ready(function() {
    var button_8 = $('button:contains("8")');
    var button_4 = $('button:contains("4")');
    var button_0 = $('button:contains("0")');
    var button_2 = $('button:contains("2")');
    var button_7 = $('button:contains("7")');
    var button_1 = $('button:contains("1")');
    var button_add = $('button:contains("+")');
    var button_sub = $('button:contains("-")');
    var button_mul = $('button:contains("*")');
    var button_div = $('button:contains("/")');
    var button_sin = $('button:contains("sin")');
    var button_tan = $('button:contains("tan")');
    var button_log = $('button:contains("log")');
    var button_clear = $('button:contains("AC")');
    var button_res = $('button:contains("=")');
    var textArea = $(".chat-text");

    QUnit.test( "Addition Test", function( assert ) {
      button_8.click();
      button_add.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 12, "8 + 4 must be 12" );
      button_clear.click();
    });
	QUnit.test( "Substraction Test", function( assert ) {
      button_8.click();
      button_sub.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 4, "8 - 4 must be 4" );
      button_clear.click();
    });

	QUnit.test("Log Test", function( assert ){
      button_1.click();
      button_0.click();
      button_0.click();
      button_log.click();
      assert.equal( $('#print').val(), 2.0000, "log(100) must be 2.0000" );
      button_clear.click();
    });

    QUnit.test("Sin Test", function( assert ){
      button_2.click();
      button_7.click();
      button_0.click();
      button_sin.click();
      assert.equal( $('#print').val(), -1.0000, "sin(90) must be -1.0000" );
      button_clear.click();
    });

    QUnit.test("Tan Test", function( assert ){
      button_0.click();
      button_tan.click();
      assert.equal( $('#print').val(), 1.0, "tan(45) must be 1.0000" );
      button_clear.click();
    });
	QUnit.test("Button Chat Box Test", function( assert ){
      $("textarea").html("okita-san");
      $(".chat-button").click();
      assert.equal( $('.msg-insert').html(),"<p class=\"messages\">okita-san</p><br>", "there's okita" );
    });
	QUnit.test("Keypress Chat Box Test", function( assert ){
      $("textarea").html("tamamo-no-mae");
      $(".chat-button").keypress(13);
      assert.equal( $('.msg-insert').html(),"<p class=\"messages\">tamamo-no-mae</p><br>", "there's tamamo" );
    });

});
